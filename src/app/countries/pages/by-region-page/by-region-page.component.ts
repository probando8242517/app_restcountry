import { Component } from '@angular/core';
import { Country } from '../../interfaces/country';


import { CountriesService } from '../../services/countries.service';
import { Region } from '../../interfaces/region.type';

// import { Region } from

@Component({
  selector: 'app-by-region-page',
  templateUrl: './by-region-page.component.html',
  styles: ``
})
export class ByRegionPageComponent {
  
  public countries : Country [] = [];

  //crear un arreglo que va tener solo tal opciones
  public regions: Region[] = ['Africa', 'America', 'Asia', 'Europe', 'Oceania'];
  public selectedRegion?: Region; 

  constructor ( private countryService : CountriesService) {}

  searchByRegion(region: Region) : void {
    this.selectedRegion = region;

   this.countryService.searchRegion( region ).subscribe( countries => {
    this.countries = countries;
   });
  }
}
