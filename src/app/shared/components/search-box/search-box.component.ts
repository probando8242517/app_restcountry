import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subject, Subscription, debounceTime } from 'rxjs';

@Component({
  selector: 'shared-search-box',
  templateUrl: './search-box.component.html',
  styles: ``
})
export class SearchBoxComponent implements OnInit, OnDestroy{



  private debouncer: Subject<string> = new Subject <string>();
  private debouncerSuscription?: Subscription;

  @Input()
  public initialValue:string = '';

  //define una propiedad que puede ser enviada desde el padre hacia el componente hijo
  @Input()
  public placeholder: string =''

  //define una salida del componente que el componente padre puede suscribirse para escuchar 
  @Output()
  public onValue = new EventEmitter<string>();

  @Output()
  public onDebounce = new EventEmitter<string>();


  ngOnInit(): void {
  this.debouncerSuscription = this.debouncer
  .pipe(debounceTime(1000))
  .subscribe( value => {
    this.onDebounce.emit(value);
  });
  }

  ngOnDestroy(): void {
   this.debouncerSuscription?.unsubscribe();
  }

  emitValue(value: string) : void{
    this.onValue.emit(value);
  }

  onKeyPress ( searchTerm: string) {
    this.debouncer.next(searchTerm);
  } 


}
